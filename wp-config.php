<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'scotchbox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'o,uYK@^{I(FZW8XX0XF^o~a]VrwG%V*mU{WQ;@`2gh?2p_BPcwU`e}Vv6@p;VF[e');
define('SECURE_AUTH_KEY',  'x,ZS9(*98]X,m]#w`5)U^c]T!#/=A<VDKw3GZDKgHg4u%`SBvL.+1nKV=?!,JPr[');
define('LOGGED_IN_KEY',    ' t7-:LXIFW>c.yTp5l&HJO$X+kq=ee_0c8k{]q$a.+N=SAW(etp7?4Yt?6|La;!{');
define('NONCE_KEY',        'C@u2(2m,Rs:Q/02CcFCrKcc#s+?V,s(O9W^<mAbzl{p)t&>MHk{),:o@1,*85adF');
define('AUTH_SALT',        '{A^5&v68,^Jm{aW$07n8V7%J9 V.JUL)PrPoK_k~b`N#)5.[]Rk7Zcc! Xq%+Gse');
define('SECURE_AUTH_SALT', ')-j{?eAe420z&KvLj;o_v1c5J.%-E,{IUT?.%M~yUcdG_UtiMc65x!uNNM{_}:mZ');
define('LOGGED_IN_SALT',   '+sJ4TN+bJNcLCY_eDgZ5G8RWAH9//c+$uz?2d?cauu|+o4&(>~xQue?9&# K?v.I');
define('NONCE_SALT',       '^DB^23GDj%:ZSSEXBdE{,p3|eoV^<rD+D9YJ_D75uXfeAjz!c>!#!=bWRoHGeBQK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
