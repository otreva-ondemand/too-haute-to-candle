// Theme elements
var $addToCartForm = jQuery('form.cart');

// Page logic
jQuery(document).ready(function() {
  localStorage.removeItem('customImage');
  jQuery('body').append($imageEditor);
  jQuery($addToCartForm).hide();
  jQuery($customizeButton).insertAfter($addToCartForm);
  initEditor();
  listenForImageUpload();
});

function listenForImageUpload() {
  document.getElementById('file').addEventListener('change', function(e) {
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onload = function (file) {
      var data = file.target.result;
      addImageToCanvas(data);
    };
    reader.readAsDataURL(file);
  });
}

function addImageToCanvas(data) {
  fabric.Image.fromURL(data, function(img) {
    faceImage = img;
    faceImage.borderColor = 'black';
    faceImage.cornerColor = 'black';
    faceImage.cornerSize = 25;
    faceImage.transparentCorners = false;
    canvas.add(faceImage).renderAll();
    canvas.setActiveObject(faceImage);
    canvas.sendToBack(faceImage);
  });
}

function openCustomizer() {
  jQuery('#overlay').css('height', '100%');
}

function closeCustomizer() {
  jQuery('#overlay').css('height', '0%');
}

function updatePreviewImageOnProductPage() {
  jQuery($productPageImagePreview).insertAfter($customizeButton);
  jQuery('#productPageImagePreview').prop('src', localStorage.getItem('customImage'));
}