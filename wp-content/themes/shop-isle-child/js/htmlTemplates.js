var $imageEditor = jQuery(
  '<div id="overlay" class="overlay">' +
  '<div class="overlayContent">' +
  '<span>Brightness:<input class="imageManipulator" type="range" id="brightness-value" value="0" min="-255" max="255"><input class="imageManipulator" type="checkbox" id="brightness"></span><br>' +
  '<span>Contrast:<input class="imageManipulator" type="range" id="contrast-value" value="0" min="-255" max="255"><input class="imageManipulator" type="checkbox" id="contrast"></span><br>' +
  '<span>Saturate:<input class="imageManipulator" type="range" id="saturate-value" value="0" min="-100" max="100"><input class="imageManipulator" type="checkbox" id="saturate"></span><br>' +
  '<span>Hue:<input class="imageManipulator" type="color" id="tint-color" value="#000000"><input class="imageManipulator" type="checkbox" id="tint">Opacity:<input class="imageManipulator" type="range" id="tint-opacity" min="0" max="1" value="0.5" step="0.1"></span><br>' +
  '<span>Grayscale:<input class="imageManipulator" type="checkbox" id="grayscale"></span><br>' +
  '<input type="file" id="file" accept="image/*">' +
  '<input type="button" id="saveImageButton" value="Save Image"><input class="imageManipulator" type="button" id="flipImageButton" value="Flip Image"><canvas id="c"></canvas>' +
  '<button onclick="closeCustomizer()">Close</button>' +
  '</div></div>'
);

var $customizeButton = jQuery('<button onclick="openCustomizer()" id="customizeButton">Customize</button>');

var $productPageImagePreview = jQuery('<img id="productPageImagePreview" src="">');