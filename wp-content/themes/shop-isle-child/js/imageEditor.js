var canvas;
var faceImage;
var template;

function initEditor() {
  var fabricFilters = fabric.Image.filters;
  var watermarkImage;
  canvas = new fabric.Canvas('c', { preserveObjectStacking:true });

  fabric.Object.prototype.padding = 5;
  fabric.Object.prototype.transparentCorners = false;
  canvas.controlsAboveOverlay = true;

  // Apply the template
  fabric.Image.fromURL('http://192.168.33.10/wp-content/uploads/template.png', function (img) {
    template = img;
    template.evented = false;
    template.selectable = false;
    template.hasBorders = false;
    template.hasControls = false;
    canvas.setDimensions({ width: template.width, height: template.height });
    canvas.add(template).renderAll();
    canvas.bringToFront(watermarkImage);
  });

  // Apply the watermark
  fabric.Image.fromURL('http://192.168.33.10/wp-content/uploads/watermark.png', function(img) {
    watermarkImage = img;
    watermarkImage.evented = false;
    watermarkImage.selectable = false;
    watermarkImage.hasBorders = false;
    watermarkImage.hasControls = false;
    canvas.add(watermarkImage).renderAll();
    canvas.bringToFront(watermarkImage);
  });

// TODO: only works for last uploaded image
// Prevent dragging images off canvas
//   canvas.on('object:moving', function (el) {
//     var obj = el.target;
//     var heightOffset = (faceImage.height * faceImage.scaleY) - 25;
//     var widthOffset = (faceImage.width * faceImage.scaleX) - 25;
//
//     // Place the object
//     obj.setCoords();
//
//     // top left
//     if (obj.getBoundingRect().top < 0 - heightOffset || obj.getBoundingRect().left < 0 - widthOffset) {
//       obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top - heightOffset);
//       obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left - widthOffset);
//     }
//
//     // bottom right
//     if (obj.getBoundingRect().top + obj.getBoundingRect().height > obj.canvas.height + heightOffset || obj.getBoundingRect().left + obj.getBoundingRect().width > obj.canvas.width + widthOffset) {
//       obj.top = Math.min(obj.top, obj.canvas.height - obj.getBoundingRect().height + obj.top - obj.getBoundingRect().top + heightOffset);
//       obj.left = Math.min(obj.left, obj.canvas.width - obj.getBoundingRect().width + obj.left - obj.getBoundingRect().left + widthOffset);
//     }
//   });

  // Sets active filters
  canvas.on('object:selected', function () {
    fabric.util.toArray(jQuery('.imageManipulator')).forEach(function (el) {
      el.disabled = false;
    });

    var filters = ['brightness', 'contrast', 'saturate', 'grayscale', 'tint'];

    for (var i = 0; i < filters.length; i++) {
      jQuery(filters[i]).checked = canvas.getActiveObject().filters[i];
    }
  });

  // Disables non-active filters
  canvas.on('selection:cleared', function () {
    fabric.util.toArray(jQuery('.imageManipulator')).forEach(function (el) {
      el.disabled = true;
    });
  });

  // Buttons
  jQuery('#saveImageButton').on('click', function () {
    canvas.remove(watermarkImage);
    var imageBase64 = canvas.toDataURL('png');
    localStorage.setItem('customImage', imageBase64);
    updatePreviewImageOnProductPage();
    closeCustomizer();
    canvas.add(watermarkImage).renderAll();
  });

  jQuery('#flipImageButton').on('click', function () {
    if (canvas.getActiveObject().get('flipX') === false) {
      canvas.getActiveObject().set('flipX', true);
    } else {
      canvas.getActiveObject().set('flipX', false);
    }
    canvas.renderAll();
  });

  // Filters
  function applyFilter(index, filter) {
    var obj = canvas.getActiveObject();
    obj.filters[index] = filter;
    obj.applyFilters(canvas.renderAll.bind(canvas));
  }

  function applyFilterValue(index, prop, value) {
    var obj = canvas.getActiveObject();
    if (obj.filters[index]) {
      obj.filters[index][prop] = value;
      obj.applyFilters(canvas.renderAll.bind(canvas));
    }
  }

  jQuery('#tint').on('click', function() {
    applyFilter(14, this.checked && new fabricFilters.Tint({
        color: jQuery('#tint-color').value,
        opacity: parseFloat(jQuery('#tint-opacity').value)
      }));
  });

  jQuery('#tint-color').on('change', function() {
    applyFilterValue(14, 'color', this.value);
  });

  jQuery('#tint-opacity').on('change', function() {
    applyFilterValue(14, 'opacity', parseFloat(this.value));
  });

  jQuery('#brightness').on('click', function () {
    applyFilter(5, this.checked && new fabricFilters.Brightness({
        brightness: parseInt(jQuery('#brightness-value').value, 10)
      }));
  });

  jQuery('#brightness-value').on('click', function () {
    applyFilterValue(5, 'brightness', parseInt(this.value, 10));
  });

  jQuery('#contrast').on('click', function () {
    applyFilter(6, this.checked && new fabricFilters.Contrast({
        contrast: parseInt(jQuery('#contrast-value').value, 10)
      }));
  });

  jQuery('#contrast-value').on('click', function () {
    applyFilterValue(6, 'contrast', parseInt(this.value, 10));
  });

  jQuery('#saturate').on('click', function () {
    applyFilter(7, this.checked && new fabricFilters.Saturate({
        saturate: parseInt(jQuery('#saturate-value').value, 10)
      }));
  });

  jQuery('#saturate-value').on('click', function () {
    applyFilterValue(7, 'saturate', parseInt(this.value, 10));
  });

  jQuery('#grayscale').on('click', function () {
    applyFilter(0, this.checked && new fabricFilters.Grayscale());
  });
}
