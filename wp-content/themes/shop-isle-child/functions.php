<?php

// Single product page
function load_single_product_page_custom_js() {
    wp_enqueue_script('fabric min js', get_stylesheet_directory_uri() . '/js/fabric.min.js');
    wp_enqueue_script('html template js', get_stylesheet_directory_uri() . '/js/htmlTemplates.js');
    wp_enqueue_script('image editor js', get_stylesheet_directory_uri() . '/js/imageEditor.js');
    wp_enqueue_script('product page js', get_stylesheet_directory_uri() . '/js/product.js');
}
add_action('woocommerce_before_single_product', 'load_single_product_page_custom_js');

?>
